export const connectToGit = async (command) => {
	

	console.log(`connectToGit: ${command}`);
	const accessToken = document.getElementById('gitAccessToken').value;
	const gitURL = document.getElementById('gitURL').value;
	const gitUserName = document.getElementById('gitUserName').value;
	const gitToMaster = document.getElementById('gitToMaster').value;
	const resp = await fetch('../connect-to-git', {
		method: 'POST',
		headers: new Headers({
			'Content-Type': 'application/json'
		}),
		body: JSON.stringify({accessToken, gitUserName, gitURL, gitToMaster, command})
	});
	
	console.log(resp);
}