<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			/* poppins-300 - latin_devanagari_latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 300;
  src: local('Poppins Light'), local('Poppins-Light'),url('/libs/fonts/poppins-300.woff2') format('woff2');
}
/* poppins-regular - latin_devanagari_latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 400;
  src: local('Poppins Regular'), local('Poppins-Regular'), url('/libs/fonts/poppins-400.woff2') format('woff2');
}
/* poppins-500 - latin_devanagari_latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 500;
  src: local('Poppins Medium'), local('Poppins-Medium'), url('/libs/fonts/poppins-500.woff2') format('woff2');
}
/* poppins-600 - latin_devanagari_latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 600;
  src: local('Poppins SemiBold'), local('Poppins-SemiBold'), url('../fonts/poppins-600.woff2') format('woff2');
}
/* poppins-700 - latin_devanagari_latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 700;
  src: local('Poppins Bold'), local('Poppins-Bold'), url('/libs/fonts/poppins-700.woff2') format('woff2');
}
/* color variables */
:root {
  --color-primary: #0067f4; /* qoom blue */
  --color-navy: #111559;
  --color-blue: #0067f4;
  --color-orange: #F28729;
  --color-red: #E84855;
  --color-yellow: #F7C660;
  --color-green: #4da422;
  --color-purple: #9c3ce9;
  
  --color-gray-10: #f4f6f7;
  --color-gray-50: #ebebeb;
  --color-gray-100: #d7d7d7;
  --color-gray-200: #c2c3c4;
  --color-gray-300: #aeafb0;
  --color-gray-400: #9a9b9c;
  
  --color-blue-10: #e6f0fe;
  --color-blue-50: #cce1fd;
  --color-blue-100: #99c2fa;
  --color-blue-200: #66a3f8;
  --color-blue-300: #3285f6;
  --color-blue-500: #0052c3;
  --color-blue-600: #003d92;
  --color-blue-700: #002961;
  
  --color-primary-10: #e6f0fe;
  --color-primary-50: #cce1fd;
  --color-primary-100: #99c2fa;
  --color-primary-200: #66a3f8;
  --color-primary-300: #3285f6;
  --color-primary-500: #0052c3;
  --color-primary-600: #003d92;
  --color-primary-700: #002961;
  
  --color-red-10: #fdedee;
  --color-red-50: #fadadd;
  --color-red-100: #f6b6bb;
  --color-red-200: #f19199;
  --color-red-300: #ed6d77;
  
  --color-purple-10: #f5ecfd;
  --color-purple-50: #ebd8fb;
  --color-purple-100: #d7b1f6;
  --color-purple-200: #c48af2;
  --color-purple-300: #b063ed;
  --color-purple-500: #7d30ba;
  --color-purple-600: #5e248c;
  --color-purple-700: #3e185d;
  
  --color-light-gray: #F4F6F7;
  --color-light-blue: #D8E6F2;
  
  --color-dark-blue: #020873;
  --color-dark-navy: #0C1440;
  
  /* text colors for light background */
  --text-dark-high: #212121;
  --text-dark-medium: #666666;
  --text-dark-disabled: #9E9E9E;

  /* text colors for dark background */
  --text-white-high: rgba(255,255,255,1);
  --text-white-medium: rgba(255,255,255,0.6);
  --text-white-disabled: rgba(255,255,255,0.38);
}

body {
	color: var(--text-dark-high);
	font-family: 'Poppins', sans-serif;
	font-weight: 300;
	margin:0;
	padding:0;
	text-rendering: optimizelegibility;
	width: 100%;
}

main {
	box-sizing: border-box;
	font-size: 16px;
	line-height: 1.25;
	margin: 0 auto 80px;
	max-width: 712px;
	padding: 0 16px;
}
h1,h2,h3,h4,h5,h6 {
	margin-block-start: 2em;
	margin-block-end: 1em;
}
/* title */
h1 {
  font-size: 36px; 
  font-weight: 500;
  padding-bottom: 16px;
  border-bottom: 1px solid var(--text-dark-high);
}
/* subheading */
h2 {
  font-size: 24px;
  font-weight: 500;
}

h3 {
  font-size: 20px;
  font-weight: 500;
}

h4, h5, h6 {
  font-size: 16px;
  font-weight: 500;
}

a {
	color: var(--color-blue);
}

p {
	hyphens: auto;
	line-height: 1.5;
	margin-block-start: 1.5em;
}
li {
	margin: 8px 0;
}
strong {
	font-weight: 500;
}

blockquote {
	background: var(--color-gray-10);
	border-left: 8px solid var(--text-dark-high);
	margin: 24px 0;
	padding: 8px 24px 16px;
	
}

hr {
	border: 0.5px solid var(--color-gray-400);
}

pre {
	background-color: var(--text-dark-high);
	border-radius: 8px;
	color: #fff;
	font-family: monospace;
	line-height: 1.5;
	overflow: auto;
	padding: 16px 24px;
}

code {
	background-color: var(--color-primary-10);
	border-radius: 4px;
	color: var(--color-primary);
	font-family: monospace;
	line-height: 20px;
	margin: 0;
	padding: 2px 6px;
	word-wrap: break-word;
}

pre > code {;
	background: transparent;
	color: #fff;
	line-height: 1.5;
	padding:0;
}

table {
	border-collapse: collapse;
	padding: 0;
}
table tr {
	border-top: 1px solid var(--color-gray-100);
	margin: 0;
	padding: 0;
}
table tr:last-child {
	border-bottom: 1px solid var(--color-gray-100);
}
table thead tr {
	border-bottom: 2px solid var(--color-gray-100);
}
table tr th {
	font-weight: 500;
	margin: 0;
	padding: 12px 16px;
	text-align: left;
}
table tbody tr:nth-child(odd) {
	background-color: var(--color-gray-10);
}
table tr td {
	margin: 0;
	padding: 8px 16px;
	text-align: left;
}

img {
	display: block;
	margin: 32px 0;
	max-width: 100%;
}
		</style>
	</head>
	<body>
		<main><h1>The Qoom Federation</h1>
<p><img src="https://jared.qoom.io/photo-1582513166998-56ed1ea02d13.png" alt="qoomfederation"></p>
<h2>The internet of today</h2>
<p>The internet of today is a world of large powerful nation states each vying for our loyality
in exchange for several benefits. It seems that we are born in a world where there are these powers that are working
hard to make all of our lives better. We accept and commit to these states so that we can not only utilize these platforms
to better ourselves and share things with each other but also to immerse ourselves in worlds that take us far from our own reality. It seems from those that
are deep in this internet of nation states, that this is the way we all want it to be. Our friends and family are all embedded in
it and thus so are we. If we do not, we are seen as weird and strange and seem to be left alone, banished into the void.</p>
<p>Instead we freely kiss the ring of the monarchs of these nation states in exchange for the awesome gifts their denizens provide us.
We do this every time we log into their site to access these gifts. We think nothing of it. But those of us who have paid attention in
history class know that the gifts will become harder and harder to receive until we are all thrown in the front-lines to fight ensuing
battles of these nation states. We will give them our virtual lives and real money to try to keep the gifts coming and exchange those
states will become more and more tyrannical. Anyone who was using the internet 15 years ago will remember we could easily go to a search engine
find what we needed without having to log in. Logging in was something to protect us, like logging into our bank accounts. Now logging in something
we do to protect these companies. That seems like a very small price to pay to
keep the internet we need humming along.</p>
<p>But unfortunately it is a slippery slope. One that will get steeper with time. Fifteen years ago
we were aghast how despotic regimes were using the internet to keep tabs of their citizens and to limit their rights and choice. Those who live
in democratic states now totally except this norm. Companies that we use and give our login info everyday now give our information to companies
that decide to use that information to see if we are worthy to buy a house or a car. Or to companies that use that information to tell us who we are.
Soon you will see that those who only can get ahead are those who walk a very thin line and obey the rules of these internet monarchs. Those living
in a democratic society will loose the right to be a king/queen of their own existence. We will loose the right to live our lives by our own
rules. We wont be able to freely choose who we want to work with or be with. All those decisions will be taken away from us.
<br><strong>So what do we do?</strong></p>
<h2>How Qoom will fill the void</h2>
<p>The void looks more and more pleasing after you see where this slope will lead us all. When there is a void other things will fill it, some worst some better. Qoom is working to fill it with a federation of humans each who can be the king/queen of their own realm once again. The original hope of the internet
was to be such a federation, like a wild west that is truly available for everyone to exploit. For an extremely cheap price we can all own a homestead through the purchase of a domain name. That was the hope. Qoom is working hard to resurrect this hope and make it vastly more convenient.
In purchasing a domain name, you get privacy protection so the public will not know that you own that domain. You can build an identity for each realm
you wish to create. You can try new things, and then give them up without consequence and then go try something new. It is up to you what you want employers, loan officers, and others to see. Each Qoom realm you create is like your
personal starship. This starship is equipped with the hardware and software needed to take you anywhere in this Qoom federation. The
software is all open source and easily editable. If you wish to make your starship look like a flying pink cat, that is your choice. No
overpaid UX designer's ego will get in the way of your unique creativity.</p>
<p>Those who created the internet created languages that you do not need
to have a computer science degree to understand. There are three languages that have been customized to make it easy for anyone to command their ship's computer. HTML and its sister markdown make it easy to put any type of content on the web, just by texting the computer what
you want to see. CSS makes it easy to make that content look great, just by describing the styles in two word sentences. Finally, Javascript is the English
language of the web. Through it you can talk not only to your starship but to other starships, spacestations, and planets that exist in the Qoom federation.
Qoom's extremely minimalistic code editor makes it simple to so that you can start to create your own version of the internet using these languages through your own starship armada at your command.
Once this federation reaches critical mass, the internet giants of today will be required to log into your ship for access and not the other way around.</p>
<h2>Next Steps</h2>
<p><strong>Qoom is just a spaceship provider</strong> you can use to blast off and escape the internet we have today. Just bring what you do to your own realm and stop doing it within the domain of the large internet giants. What you do on your domain/realm is up to you! Create your domain at: <a href="https://www.qoom.io">www.qoom.io</a></p>
<h3>Welcome to the void and be as free and creative as you wish to be!</h3>
</main>
	</body>
</html>